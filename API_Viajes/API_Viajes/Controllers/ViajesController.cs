﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_Viajes.Models;

namespace API_Viajes.Controllers
{
    public class ViajesController : ApiController
    {
        private AgenciasDB db = new AgenciasDB();

        // GET: api/Viajes
        public IQueryable<Viajes> GetViajes()
        {
            return db.Viajes;
        }

        // GET: api/Viajes/5
        [ResponseType(typeof(Viajes))]
        public IHttpActionResult GetViajes(int id)
        {
            Viajes viajes = db.Viajes.Find(id);
            if (viajes == null)
            {
                return NotFound();
            }

            return Ok(viajes);
        }

        // PUT: api/Viajes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutViajes(int id, Viajes viajes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != viajes.ID_Viajes)
            {
                return BadRequest();
            }

            db.Entry(viajes).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ViajesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Viajes
        [ResponseType(typeof(Viajes))]
        public IHttpActionResult PostViajes(Viajes viajes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Viajes.Add(viajes);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = viajes.ID_Viajes }, viajes);
        }

        // DELETE: api/Viajes/5
        [ResponseType(typeof(Viajes))]
        public IHttpActionResult DeleteViajes(int id)
        {
            Viajes viajes = db.Viajes.Find(id);
            if (viajes == null)
            {
                return NotFound();
            }

            db.Viajes.Remove(viajes);
            db.SaveChanges();

            return Ok(viajes);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ViajesExists(int id)
        {
            return db.Viajes.Count(e => e.ID_Viajes == id) > 0;
        }
    }
}