﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_Viajes.Models;

namespace API_Viajes.Controllers
{
    public class ViajeroController : ApiController
    {
        private AgenciasDB db = new AgenciasDB();

        // GET: api/Viajero
        public IQueryable<Viajero> GetViajero()
        {
            return db.Viajero;
        }

        // GET: api/Viajero/5
        [ResponseType(typeof(Viajero))]
        public IHttpActionResult GetViajero(int id)
        {
            Viajero viajero = db.Viajero.Find(id);
            if (viajero == null)
            {
                return NotFound();
            }

            return Ok(viajero);
        }

        // PUT: api/Viajero/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutViajero(int id, Viajero viajero)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != viajero.ID_Viajero)
            {
                return BadRequest();
            }

            db.Entry(viajero).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ViajeroExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Viajero
        [ResponseType(typeof(Viajero))]
        public IHttpActionResult PostViajero(Viajero viajero)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Viajero.Add(viajero);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = viajero.ID_Viajero }, viajero);
        }

        // DELETE: api/Viajero/5
        [ResponseType(typeof(Viajero))]
        public IHttpActionResult DeleteViajero(int id)
        {
            Viajero viajero = db.Viajero.Find(id);
            if (viajero == null)
            {
                return NotFound();
            }

            db.Viajero.Remove(viajero);
            db.SaveChanges();

            return Ok(viajero);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ViajeroExists(int id)
        {
            return db.Viajero.Count(e => e.ID_Viajero == id) > 0;
        }
    }
}