﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API_Viajes.Models;

namespace API_Viajes.Controllers
{
    public class Viaje_VController : ApiController
    {
        private AgenciasDB db = new AgenciasDB();

        // GET: api/Viaje_V
        public IQueryable<Viaje_V> GetViaje_V()
        {
            return db.Viaje_V;
        }

        // GET: api/Viaje_V/5
        [ResponseType(typeof(Viaje_V))]
        public IHttpActionResult GetViaje_V(int id)
        {
            Viaje_V viaje_V = db.Viaje_V.Find(id);
            if (viaje_V == null)
            {
                return NotFound();
            }

            return Ok(viaje_V);
        }

        // PUT: api/Viaje_V/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutViaje_V(int id, Viaje_V viaje_V)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != viaje_V.ID_Viaje_V)
            {
                return BadRequest();
            }

            db.Entry(viaje_V).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Viaje_VExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Viaje_V
        [ResponseType(typeof(Viaje_V))]
        public IHttpActionResult PostViaje_V(Viaje_V viaje_V)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Viaje_V.Add(viaje_V);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = viaje_V.ID_Viaje_V }, viaje_V);
        }

        // DELETE: api/Viaje_V/5
        [ResponseType(typeof(Viaje_V))]
        public IHttpActionResult DeleteViaje_V(int id)
        {
            Viaje_V viaje_V = db.Viaje_V.Find(id);
            if (viaje_V == null)
            {
                return NotFound();
            }

            db.Viaje_V.Remove(viaje_V);
            db.SaveChanges();

            return Ok(viaje_V);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Viaje_VExists(int id)
        {
            return db.Viaje_V.Count(e => e.ID_Viaje_V == id) > 0;
        }
    }
}